/**
 * Check if the number is an integer (0-255).
 * @param {integer} value - value to be checked.
 * @returns {boolean} 
 */
 const checkNumber= (value)=>{
  value = parseInt(value);
  if(value < 0 || value > 255 || isNaN(value)){
  return false;
  }
  return true;
}

/**
 * Convert number to a hexadecimal (base 16).
 * @param {integer} number - Number to be converted.
 * @returns {string} hexadecimal value.
 */
const numberToHex = (number) => {
  number = parseInt(number);
  var hex = number.toString(16).toUpperCase();
  return hex.length == 1 ? "0" + hex : hex;

}

/**
 * Convert RGB to a hexadecimal (base 16).
 * @param {integer} red - value for red to be converted.
 * @param {integer} green - value for green to be converted.
 * @param {integer} blue - value for blue to be converted. 
 * @returns {string} Hexadecimal value based on RGB | Error message.
 */
export const rgbToHex =(red, green, blue) => {
  if(checkNumber(red) & checkNumber(green) & checkNumber(blue))
  return '#' + numberToHex(red) + numberToHex(green) + numberToHex(blue);
  return  'Impossible, verify your entries Integer(0-255)'
}

/**
 * Convert hexadecimal to RGB.
 * @param {integer} red - Hexadecimal (6 digits).
 * @returns {object} Rgb .
 */

export const hexToRgb = (hex)=>{
  if (hex.length != 6) {
    throw "Only six-digit hex colors are allowed.";
  }

  var RgbHex = hex.match(/.{1,2}/g);
  var Rgb = {
    'red': parseInt(RgbHex[0], 16),
    'green': parseInt(RgbHex[1], 16),
    'blue': parseInt(RgbHex[2], 16)
  }
  return Rgb;
}

  