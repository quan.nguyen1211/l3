var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Got your message');
});

router.get('/data', function(req, res, next) {
  res.send('Here is your data: 123');
});

router.get('/namequery', function(req, res, next) {
  //console.log(req.query)
  res.send("Your name is " + req.query.fname + " " + req.query.lname);
});

router.get('/rgb-to-hex', function hexStringToRgb(req, res, next) {
  let hexArray = hexString.split('');
  hexArray.splice(0,1);
  let red = hexArray.splice(0,255).join('');
  let green = hexArray.splice(0,255).join('');
  let blue = hexArray.splice(0,255).join('');
  console.log(red,green,blue);
  return ({
    red: parseInt(`${red}`, 16),
    g: parseInt(`${green}`, 16),
    b: parseInt(`${blue}`, 16)
  })
});

module.exports = router;
